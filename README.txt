Welcome to LeatherStore20!


//Introduction//
This project is a collaborative effort to create a tool for a leather store to use a MySQL database backend powered by Java code. There is no GUI, however, based on the way the program is structured, a front-end could easily be adapted to give users a GUI. The application is run as a Java application via terminal across Windows, Linux or any other OS that uses a terminal. The major benefit of this approach is the Connector/J library that is built for MySQL. The documentation and community support for this particular relational database is very detailed, and solving most issues with debugging whether they related to queries or Connector/J was fairly simple. 


//Instructions://

The first step for running LeatherStore20 is the same as any other .jar application. Open up your favorite terminal application and navigate to the folder where the file is located. Upon doing so, execute the program by typing in

"java -jar LeatherStore20.jar"

Upon doing so, you'll be greeted with a prompt for user name and password. We've added two accounts to demonstrate the integration between our LeatherStore database and the MySQL provided user table. For the admin, a user account for ordering would most likely not be necessary. However, the user account cross-checks against user e-mails in the User table, making sure that the user is already in the system at log-in time. Let's say our user's name is Mr. Smith. He would enter his username, derived from his e-mail address, mrsmith@gmail.com, which would be mrsmith. His password is 1234. Go ahead and enter those into the prompts.

If you typed in something wrong, you will be denied due to invalid credentials and prompted again smoothly. When you do enter, the user's ability is limited, as you can see. The user is presented with the ability to view and quit. They may not alter database information. Upon pressing 1, the user is taken to their viewing menu, which gives three options: My Info, My Orders, and to Go Back. My Info will bring up the user's account information. My orders will bring up the orders that the user has placed. Now, quit out as the user and we'll delve into the admin options.

The admin account is not read from the customers table, but from the MySQL user table. This prevents logins from users even in case of bugs from reading admin passwords because they are stored on a different table. For our admin login, we used team20 with a password of cs425. Upon using this login, the administrator is greeted with an entirely different menu. There are four options: Add, Modify, Delete and View. Each of these options has its own particular set of features.

The first option is add. This feature allows the user to select a table from the database's list and add a new entry by inputting each field. At the end, the option is given to add another entry, in case of multiple entries or to quit. When each entry is popped up for the user to add, the program displays a hint as to what goes into that field.

The second option is modify. This feature allows the user to update attributes in any of the tables.  First, choose a table.  Then select the attributes that you wish to modify.  Enter the new values for each attribute as prompted.  Finally, follow the prompts to choose which attribute you would like to use as a constraint and enter that constraint.

The next option is delete. Delete has many built in functions for your convenience. Whenever you see '>' that means the program is waiting on you for a response. Delete Orders by date allows you to delete all orders older than your choice of date. If you need more advanced deletion methods, option 5 from the delete menu will enter advanced delete mode. You can also press 6 to return to a previous level. If you input anything other than an integer without entering advanced mode, you will be returned to the previous menu and your command will be ignored.
Option 5 of delete enters advanced delete mode. Advanced delete mode will list all tables in existence, you then enter the table name you would like to delete from and follow the prompts. You can drop tables, delete specific entries, or delete the contents of a table. If you try to delete a value that is critical to the integrity of another table, you will recieve an explanation of which table has the reference and the attribute that references the field. You will need to delete those first before finishing your original delete, this is to prevent accidental deletion
If you delete a category, all subcategories will also be deleted.

The final displayed option is View. This feature allows the administrator to see the information contained in the database in a myriad of ways. If you're familiar with SQL, you can tell that it is building a query. However, it's abstract enough to where any user can quickly pick up how it works. The first menu displayed shows a list of tables containing different information. There is also an Additional Options menu for more complicated operations that give the user details they might want to know about leather store sales. These are self explanatory. Delving into a table, the information about each entity is printed. The user can choose a custom set of columns or all to receive their information. Upon entering the columns, an admin receives choices of whether or not they want to implement a function, whether they want their data grouped or sorted, and whether or not they want to add a constraint. These are representative of the aggregate functions, group by and order by clauses, and where clause. Once all this is done, the results are concatenated to a query and sent to the server, which then will serve up custom information as a list based on the query. Should the query fail, the program will output a message and send the user back to the main view menu. Using this menu of options, the user can acquire a broad stroke of all the data in a table all the way to a custom group of items based on a more complex function.

This covers all of the main menu. There is, however, a secret command! If you enter in the following command, you will erase the database and repopulate it with default values. To do this, you type in:

"There is no spoon"

Some intimidating text pops up, and then you're greeted with the menu like nothing happened. That is because our GECK(Garden of Eden Kit) script has run. This script has functions such as NuclearWinter, and AdamEve that destroy and rebuild(respectively!) the default database. This command, along with the classes it uses, serve as a script to build/destroy/rebuild the database. This is our delivery of that particular deliverable.



//ERD//
The ERD has some changes because in implementation it made more sense to have address as its own relationship to handle seperate billing and shipping address.
The dbms also had a user table built in so we did not need to create one of our own.
Some fields were added to the products table to match realistic expectations.
Some fields were renamed to prevent confusion about multiple ids or multiple description fields.
