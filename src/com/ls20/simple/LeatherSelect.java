package com.ls20.simple;


/**
 * Created by Oless on 4/15/2014.
 */
import java.sql.*;

import java.util.Scanner;

public class LeatherSelect {
    private Statement statement = null; //statement that will be built
    private ResultSet resultSet = null; //result set used in many methods

    //ag & gs are flags that change how the string is concatenated and what options they show up as. They stand for AGgregate and GroupSort.
    int ag;
    int gs;


    //This method takes in the customer's entry whether it's their information or orders and promptly transacts it with the database then prints to terminal.
    public void spitOutDataCustomer(String tableName, Connection conn, int userId ) throws Exception {

        statement = conn.createStatement();
        resultSet = statement.executeQuery("select * from LEATHERSTORE." + tableName + " WHERE User_ID = " + Integer.toString(userId) + ";");
        writeResultSet(resultSet);

    }


    //This is the main method to view data. It begins by creating a statement and then using a quick select, it gets more data and builds a query to a table that's more specific.
    public void spitOutData(String tableName, Connection conn, Scanner scan) throws Exception {
        statement = conn.createStatement(); //create the statement using the passed in connection.
        resultSet = statement.executeQuery("select * from LEATHERSTORE." + tableName + ";"); // Get information from the table
        ResultSetMetaData rsmd = resultSet.getMetaData(); //Acquire Metadata Information



        System.out.println("Which information would you like? Enter names comma separated, please. If you'd like to see all columns, just enter \"all\""); //Prompt user for inputs, print columns and prompt.
        for (int i = 1; i <= rsmd.getColumnCount(); i++) {
            System.out.println(rsmd.getColumnName(i));
        }
        System.out.print("> ");
        String cols = scan.nextLine();
        if (cols.trim().toLowerCase().equals("all")) { //special case for if they want all columns. Easier than typing all of them in
            cols = "*";
        }



        String[] wants = cols.split(",");  //Splits and trims to eliminate odd whitespace
        for (int i = 0; i < wants.length; i++) {
            wants[i] = wants[i].trim();
        }



        //Clean up input and create a clean columns string for the non-aggregate case.
        String finalcols = "";
        finalcols=finalcols.concat(wants[0]);
        for (int i = 1; i < wants.length; i++) {
            finalcols = finalcols.concat(", " + wants[i]);
        }

        //Create Strings by calling Aggregates, make initial(non-aggregate) and prompt for grouping or sorting
        String aggrcols=Aggregates(wants, scan);
        String queryString="select " + finalcols + " from leatherstore." + tableName+" ";
        String groupSort=groupBySortBy(rsmd, queryString, wants, scan);

        //Present the user with an option to add a where clause condition
        System.out.println("Would you like to add a condition?\n Yes \n No");
        System.out.print("> ");
        //using flags and response to where, create string for query
        if ( scan.nextLine().charAt(0) == 'y' ) { // If they do want a where clause
            if (ag == 1 && gs == 1) { //Case: with where, aggregate and grouping
                queryString = "select " + aggrcols + " from leatherstore." + tableName + " " + groupSort + " " + WhereClause.makeWhereClause(conn, scan, rsmd);
                queryString = queryString.replace("where", "having");
            } else if (ag == 1 && gs == 0) { //case: aggregate, no grouping and where
                queryString = "select " + aggrcols + " from leatherstore." + tableName + " " + WhereClause.makeWhereClause(conn, scan, rsmd);
            } else if (ag == 0 && gs == 0) {// case: with where, but no grouping or aggregate
                queryString = "select " + finalcols + " from leatherstore." + tableName + " " + WhereClause.makeWhereClause(conn, scan, rsmd);
            } else if (ag == 0 && gs == 1) { //case: no aggregate, sorted with a where.
                queryString = "select " + finalcols + " from leatherstore." + tableName + " " + WhereClause.makeWhereClause(conn, scan, rsmd) + " " + groupSort;
            }
        }
        else{//In case a where clause is not included
            if (ag==1 & gs==1){ //case: aggregate, grouped, no where
                queryString="select " + aggrcols + " from leatherstore." + tableName+" " + groupSort;
            }
            else if (ag==1 && gs==0){ //case: aggregate, no grouping, no where
                queryString="select " + aggrcols + " from leatherstore." + tableName;
            }
            else if (ag==0 && gs==0){  //case: no grouping, no aggregate, no where
                queryString="select " + finalcols + " from leatherstore." + tableName+" ";
            }
            else if (ag==0 && gs==1){  //case: sorted, with no where and no aggregates.
                queryString="select " + finalcols + " from leatherstore." + tableName+" "+groupSort;
            }
        }
        //cap off the string with a semicolon and execute the query, then print result set to terminal.
        queryString=queryString+";";
        ResultSet results = statement.executeQuery(queryString);
        writeResultSet(results);
    }

    //Assess whether or not to add aggregate function
    public String Aggregates(String[] cols, Scanner scan) throws Exception {
        //agflag flags ag depending on method output, function stores user's choice
        int agflag=0;
        int function;

        //Prompt user, deliver choices
        System.out.println("Would you like a function of one of these columns?");
        System.out.print("> ");
        String entry=scan.nextLine();

        //if user wants to add a condition
        if (entry.equals("yes") || entry.equals("y") || entry.equals("1")) {


            //if they chose a specific set of options they get more choices
            if (!cols[0].trim().equals("*")) {
                //print through the columns they want
                System.out.println("Which Item would you like a function of?");
                for (int i = 0; i < cols.length; i++) {
                    System.out.println(cols[i]);

                }
                System.out.print("> ");
                entry = scan.nextLine();
            }
            //Ask what function to apply, check if all, in which case only count is available, otherwise, print all 7 functions supported
            System.out.println("Which function would you like to apply?");

            if(cols[0].trim().equals("*")){

                System.out.println(" 1. Count");
                entry="*";
                System.out.print("> ");
                function=Integer.parseInt(scan.nextLine()); //store user input in function

            }

            else {
                System.out.println(" 1. How many?\n 2. Average\n 3. First\n 4. Last\n 5. Max\n 6. Min\n 7.Sum");
                System.out.print("> ");
                function = Integer.parseInt(scan.nextLine()); //store user input in function
            }

            if(cols[0].trim().equals("*") & function!=1){ //catch improper inputs in case of all

                function=9;
            }
            switch (function) { //switch based on what function they choose. Find the column they wanted to aggregate on and wrap it in a function, set the flag and break
                case 1:
                    for (int i = 0; i < cols.length; i++) {
                        if (cols[i].equals(entry.trim().toLowerCase())) {
                            cols[i] = "count(" + cols[i] + ")";
                        }

                    }
                    agflag = 1;
                    break;
                case 2:
                    for (int i = 0; i < cols.length; i++) {
                        if (cols[i].equals(entry.trim().toLowerCase())) {
                            cols[i] = "avg(" + cols[i] + ")";
                        }

                    }
                    agflag = 1;
                    break;
                case 3:
                    for (int i = 0; i < cols.length; i++) {
                        if (cols[i].equals(entry.trim().toLowerCase())) {
                            cols[i] = "first(" + cols[i] + ")";
                        }

                    }
                    agflag = 1;
                    break;
                case 4:
                    for (int i = 0; i < cols.length; i++) {
                        if (cols[i].equals(entry.trim().toLowerCase())) {
                            cols[i] = "last(" + cols[i] + ")";
                        }

                    }
                    agflag = 1;
                    break;
                case 5:
                    for (int i = 0; i < cols.length; i++) {
                        if (cols[i].equals(entry.trim().toLowerCase())) {
                            cols[i] = "max(" + cols[i] + ")";
                        }

                    }
                    agflag = 1;
                    break;
                case 6:
                    for (int i = 0; i < cols.length; i++) {
                        if (cols[i].equals(entry.trim().toLowerCase())) {
                            cols[i] = "min(" + cols[i] + ")";
                        }

                    }
                    agflag = 1;
                    break;
                case 7:
                    for (int i = 0; i < cols.length; i++) {
                        if (cols[i].equals(entry.trim().toLowerCase())) {
                            cols[i] = "sum(" + cols[i] + ")";
                        }

                    }
                    agflag = 1;
                    break;
                default: //in case of improper input: do nothing.
                    break;

            }

            //Set up string cleanly for output same as in spitOutData
            String finalcols = "";
            finalcols=finalcols.concat(cols[0]);
            if(cols.length>1) {
                for (int i = 1; i < cols.length; i++) {
                    finalcols = finalcols.concat(", " + cols[i]);
                }
            }

            //set flag and return string
            ag=agflag;
            return finalcols;
        }
        else{
            //Clean up input again, just in case, even though nothing was done
            String finalcols = "";
            finalcols=finalcols.concat(cols[0]);
            if(cols.length>1) {
                for (int i = 1; i < cols.length; i++) {
                    finalcols = finalcols.concat(", " + cols[i]);
                }
            }
            //set flag in case it was tripped by something being broken, and return string with no aggregators
            ag=0;
            return finalcols;

        }


    }

    //Function to prompt the user to add a grouping or sorting column to their query
    public String groupBySortBy(ResultSetMetaData rsmd, String initQuery, String[] cols, Scanner scan) throws Exception  {
        try {
            //Set up command based on aggregator flags
            String command="Sort";
            if(ag==1){
                command="Group";
            }
            //If a count of all is implemented this method breaks out quickly and is ignored
            if (ag==1 && cols[0].trim().equals("*")){
                return "";
            }

            //Print prompt for user, take in user input
            System.out.println("Do you want to "+command+" your results?\n 1. yes\n 2. no");
            System.out.print("> ");
            String entry = scan.nextLine();

            //Give user plenty of ways to say yes.
            if (entry.equals("1") || entry.equals("group") || entry.equals("sort") || entry.equals("yes") || entry.equals("y")) {
                //Give informative messages while setting flag
                System.out.println("You asked to "+command+".");
                gs=1;
                System.out.println("Please enter columns by which to "+command+", separated by commas");
                //print out all sought columns, but not the one with the aggregator
                for (int i = 0; i < cols.length; i++) {
                    if(!cols[i].contains("("))
                        System.out.println(cols[i]);
                }
                //prompt/take in input
                System.out.print("> ");
                entry = scan.nextLine();


                //Split up into String array for manipulation, and clean up input.
                String[] wants = entry.split(",");
                for (int i = 0; i < wants.length; i++) {
                    wants[i] = wants[i].trim().toLowerCase();
                }
                //Case for all:make sure that user inputs are in the set of columns for the table
                if (cols[0].equals("*")) {

                    //set up boolean flags
                    boolean correctcheck = true;
                    boolean singular = false;

                    //iterate through input strings, compare with columns. If an input string is not in there, set flag
                    for (int j = 0; j < wants.length; j++) {
                        for (int i = 1; i <= rsmd.getColumnCount(); i++) {
                            if (rsmd.getColumnName(i).trim().toLowerCase().equals(wants[j].trim().toLowerCase())) {
                                singular = true;
                            }

                        }
                        if (!singular) {
                            correctcheck = false;
                        }
                        singular = false;

                    }

                    //concatenate string array back into single string
                    String finalcols = "";
                    finalcols =finalcols.concat(wants[0]);
                    if(wants.length>1) {
                        for (int i = 1; i < wants.length; i++) {
                            finalcols = finalcols.concat(", " + wants[i]);
                        }
                    }
                    //return statement for use if all data is properly formatted and contained in the set of columns.
                    if (correctcheck) {
                        if (command.equals("Sort")){
                            command="Order";
                        }
                        return command+" By " + finalcols + " ";
                    }

                    //Otherwise, if the user did not select all, but a subset of columns
                } else {
                    //set up flags
                    boolean correctcheck = true;
                    boolean singular = false;

                    //iterate through user's wanted data set and make sure they chose valid columns
                    for (int j = 0; j < wants.length; j++) {
                        for (int i = 1; i < cols.length; i++) {
                            if (cols[i].trim().toLowerCase().equals(wants[j].trim().toLowerCase()))
                                singular = true;

                        }
                        if (!singular) {
                            correctcheck = false;
                        }
                        singular = false;

                    }



                    //Concatenate string nicely
                    String finalcols = "";
                    finalcols =finalcols.concat(wants[0]);
                    if(wants.length>1) {
                        for (int i = 1; i < wants.length; i++) {
                            finalcols = finalcols.concat(", " + wants[i]);
                        }
                    }


                    //If all data is properly input, return statement for use in query
                    if (correctcheck) {
                        if (command.equals("Sort")){
                            command="Order";
                        }
                        return command+" By " + finalcols + " ";
                    }
                }
            }

            //If user doesn't want to or presses anything but yes, flag is not set, initial query comes out.
            else {
                gs=0;
                return initQuery;

            }
        }
        catch(Exception e){
            //Try catch to allow the user to pass through method and continue query if yes is accidentally selected.
            gs=0;
            return initQuery;
        }

        //final return method in case somehow nothing else works out. This is a last-ditch default case.
        return initQuery;
    }

        //additional options for specific, useful, and complex queries.
    public void additionalMenu(Scanner scan, Connection conn) throws Exception {
        try {

            //prompt and take in data
            statement=conn.createStatement();
            System.out.println("1. Get Average Order Price\n2. Get Orders in Chronological order\n3. Get Customer Order Totals\n4. Get Customers by Region");
            System.out.print("> ");
            int selection=Integer.parseInt(scan.nextLine());


            //Depending on which option is chosen, a query is executed and its results are printed to terminal.
            switch(selection){
                case 1:
                    resultSet=statement.executeQuery("select avg(Total_Price) from leatherstore.orders");
                    resultSet.first();
                    writeResultSet(resultSet);
                    break;



                case 2:
                    System.out.println("Would you like these 1. Ascending or 2. Descending?");
                    System.out.print("> ");
                    int sel2=scan.nextInt();
                    String order="Asc";
                    if (sel2==2)
                        order="Desc";
                    resultSet=statement.executeQuery("select * from leatherstore.orders order by order_date "+order);
                    writeResultSet(resultSet);
                    break;



                case 3:
                    System.out.println("Would you like these 1. Ascending or 2. Descending?");
                    System.out.print("> ");
                    int sel3=scan.nextInt();
                    order="Asc";
                    if (sel3==2)
                        order="Desc";
                    resultSet=statement.executeQuery("select kostomers.first_name, kostomers.last_name, sum(order.total_price) from leatherstore.kostomers right join leatherstore.orders on order.user_id=kostomers.user_id group by order.user_id order by sum(order.total_price)"+order);
                        writeResultSet(resultSet);
                    break;



                case 4:
                    System.out.println("Would you like these 1. By Zip Code or 2. By City or 3. By State?");
                    System.out.print("> ");
                    int sel4=scan.nextInt();
                    String area="zipcode";
                    if (sel4==2)
                        area="City";
                    if (sel4==3)
                        area="State";
                    resultSet=statement.executeQuery("select address."+area+", count(kostomers.user_id) from kostomers left join address on kostomers.primary_shipping_address=address.address_id group by address."+area+" order by count(kostomers.user_id);");
                    writeResultSet(resultSet);

                    break;

                //in case of invalid input, does nothing.
                default:
                    break;
            }
        }
        catch(Exception E){
            E.printStackTrace();
        }
    }
        //A quick call to make printing out result sets easily. See printResults for more information.
    private void writeResultSet(ResultSet resultSet) throws Exception {

        TablePrint.printResults(resultSet);

    }

}