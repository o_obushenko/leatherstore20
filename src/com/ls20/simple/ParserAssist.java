package com.ls20.simple;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.DatabaseMetaData;
/**
 * Created by Th3 D1v1n3 5h4d0w (Bill Molchan) on 4/30/2014.
 * This is a collection of common outputs or frequently called functions that may be used by multiple classes
 *
 */
public class ParserAssist {
//put general navigation here
    public static void printLine(String printme){System.out.println(printme);}
    public void badchoice(){printLine("Your input could not be understood \t Please use numbers only");}
    public void menuInstructions(){printLine("Please Input a number from the menu");}
    public void canceled(){printLine("returning to previous menu");}
    public void prompt(){System.out.print(">");}

    //delete specific navigaton
    public void promptCustomer(){printLine("Please enter kostomer ID, or 0 to cancel");}
    public void customerNotFound(){printLine("That kostomer ID was not found");}
    public void orderNotFound(){printLine("That order ID was not found");}
    public void tableListIntro() {printLine("These are all the available tables:");}
    public void promptTable() {printLine("Please select the table you would like to delete from");prompt();}
    public void tableNotFound() {printLine("The table you entered was not found, enter any key to continue");}
    public void promptExpireAge() {printLine("Delete orders older than how many days? (Full days only)");prompt();}
    public void warningOrder() {printLine("Warning! This will also delete the customer's history");}

    //flags
    public static final int parsefailFlag = -1;
    public static final int cancelFlag = -2;
    public static final int notFound = -3;
    public static final String stringFailFlag = "EpicFail";

    //Methods to Output Data
    public static String showTable(String tableName, Connection c) {
        try {
            Statement statement = c.createStatement();
            ResultSet results = statement.executeQuery("select * from leatherstore." + tableName + ";");
            TablePrint.printResults(results);
            return "success";
        } catch (Exception e) {
            return "Table not Found" + e.getMessage();
        }
    }

    //nifty function that gets a resultset of all the tables in the database
    public static ResultSet getTables(Connection c){

        try {
            Statement statement = c.createStatement();
            return statement.executeQuery("SELECT TABLE_NAME\n" +
                    "FROM information_schema.TABLES \n" +
                    "WHERE TABLE_SCHEMA = 'leatherstore'");}
        catch (Exception e) {            return null;}

    }

    //prints all the table names in the database to the terminal
    public static String listTables(Connection c) {
            ResultSet results= getTables(c);
            try {
                while (results.next()) {
                    System.out.println(results.getString("Table_Name"));
                }
                return "success";
            }
            catch (Exception e){return "Schema error";}//should never occur unless database is in error
        }

    //This function finds the primary key of a table.
    //it is used for the delete functions
    //only works for tables with one primary key
    public static String primaryKeyFinder(String tableName, Connection c)    {   try {
        DatabaseMetaData dm = c.getMetaData();
        ResultSet rs=dm.getPrimaryKeys(null,null,tableName);
        rs.first();
        return rs.getString("COLUMN_NAME")+" ";
    }
    catch (Exception e){return e.getMessage();}
    }

    //This function executes a query returning any errors
    public static String executeCommand(Connection c, String command){

        try {
            Statement statement = c.createStatement();
            statement.executeUpdate(command);
            return command +" Executed \n\n";
        }
        catch (Exception e) {            return e.getMessage();}

    }


}
