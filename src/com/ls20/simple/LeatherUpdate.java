package com.ls20.simple;

/**
 * Created by Alex on 4/29/2014.
 */

import java.sql.*;
import java.util.Scanner;
import java.util.Date;

public class LeatherUpdate {
    private Statement statement = null;
    private ResultSet resultSet = null;

    public void processUpdate(Scanner sc, Connection conn) throws Exception{
        Class.forName("com.mysql.jdbc.Driver");
        statement=conn.createStatement();

        TablePrint tp = new TablePrint();               //prints list of tables
        tp.printTables(conn);

        System.out.println("Which table would you like to update?");        //selects which table to update
        System.out.print("> ");
        int selection = Integer.parseInt(sc.nextLine()) - 1;
        String tableName = tp.getTableName(selection);

        //user chooses id of tuple to modify and with attributes to modify
        resultSet=statement.executeQuery("select * from LEATHERSTORE."+tableName+";");
        ResultSetMetaData rsmd=resultSet.getMetaData();
        System.out.println("Which columns would you like to change? Enter names comma separated, please.");
        for(int i=1; i<=rsmd.getColumnCount();i++){
            System.out.println(rsmd.getColumnName(i));
        }
        System.out.print("> ");

        String cols=sc.nextLine();              //scans list of attributes
        String[] wants=cols.split(",");         //breaks up attributes into array
        for(int i=0;i<wants.length;i++){
            wants[i]=wants[i].trim();
        }
        //indexes the attributes chosen
        int[] index = new int[wants.length];
        for(int i=0;i<wants.length;i++){
            for(int j=1;j<rsmd.getColumnCount()+1;j++){
                if(wants[i].equals(rsmd.getColumnName(j))){
                    index[i]=j;
                }
            }
        }
        int columnType;
        //user enters new values
        String[] newVals = new String[wants.length];
        for(int i=0;i<wants.length;i++){
            System.out.println("Please enter the new value for "+wants[i]);
            System.out.print(">");
            newVals[i]=sc.nextLine().trim();                                            //scans in new values for requested attributes
            columnType=rsmd.getColumnType(index[i]);
            if((!isInteger(newVals[i]) && columnType==Types.INTEGER)||                  //ensures no string is entered for a numeric attribute
                    (!isDouble(newVals[i]) && columnType==Types.DOUBLE)||
                        (!isDouble(newVals[i]) && columnType==Types.DECIMAL)) {
                System.out.println("Invalid input, please try again");
                i--;
            }
        }
        //creates SET portion of query
        String setNew="";
        setNew=setNew.concat(wants[0]+"=");
        columnType = rsmd.getColumnType(index[0]);
        if (columnType == Types.LONGNVARCHAR || columnType == Types.LONGVARCHAR ||                              //add ' ' if string
                columnType == Types.VARCHAR || columnType == Types.NVARCHAR || columnType == Types.DATE) {
            setNew += "'";
            setNew += newVals[0];
            setNew += "'";
        } else {
            setNew += newVals[0];
        }

        for(int i=1;i<wants.length;i++){
            setNew += ", ";
            setNew += wants[i];
            setNew += "=";
            columnType = rsmd.getColumnType(index[i]);
            if (columnType == Types.LONGNVARCHAR || columnType == Types.LONGVARCHAR ||
                    columnType == Types.VARCHAR || columnType == Types.NVARCHAR || columnType == Types.DATE) {
                setNew += "'";
                setNew += newVals[i];
                setNew += "'";
            } else {
                setNew += newVals[i];
            }
        }

        String where = WhereClause.makeWhereClause(conn,sc,rsmd);                   //builds where clause
        //executes update
        try {
            //System.out.println("UPDATE leatherstore."+tableName+ " SET " + setNew + " " + where);
            statement.executeUpdate("UPDATE leatherstore."+tableName+ " SET " + setNew + " " + where + ";");
        } catch (Exception e){
            System.out.println("Error in query");
        }
    }

    public static boolean isInteger(String s) throws Exception{
        try {
            Integer.parseInt(s);
        } catch(NumberFormatException e) {
            return false;
        }
        return true;
    }

    public static boolean isDouble(String s) throws Exception{
        try {
            Double.parseDouble(s);
        } catch(NumberFormatException e) {
            return false;
        }
        return true;
    }
}
