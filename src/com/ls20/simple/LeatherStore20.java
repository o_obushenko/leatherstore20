package com.ls20.simple;

import com.mysql.jdbc.MySQLConnection;
import com.ls20.simple.LeatherSelect;
import sun.swing.StringUIClientPropertyKey;

import java.sql.DriverManager;
import java.util.Scanner;
import java.sql.Connection;
import java.sql.*;

/**
 * Created by Oless on 4/14/2014.
 */

//Clement
//Alex
    //Bill

public class LeatherStore20 {
    static String replPrompt = "> ";
    //changes
    static String menu = "What would you like to do?\n" +
            " 1. Add" +
            "\t2. Modify" +
            "\t3. Delete" +
            "\t4. View" +
            "\tq. quit\n";
    private static String menuCustomer = "What would you like to do?\n" +
            "1. View\n" +
            "q. quit\n";


    public static void main(String[] args){

        LeatherSelect myLS=new LeatherSelect();
        LeatherDelete terminator=new LeatherDelete();
        LeatherUpdate change = new LeatherUpdate();
        Scanner sc = new Scanner(System.in);

        String username = null;
        String password = null;
        try {
        Connection connect = null;
            //A loop that prompts the user for credentials, and then uses those credentials to connect to the MySQL database
        while ( connect == null ) {
                try {
                    System.out.print("username: ");
                    username = sc.nextLine();
                    System.out.print("password: ");
                    password = sc.nextLine();
                    connect = DriverManager.getConnection("jdbc:mysql://198.37.25.133:3306/leatherstore?" +
                            "user=" + username + "&" + "password=" + password);
                }
                catch(Exception E) {
                    System.out.println("Invalid credentials, please try again...");
                }
        }



        boolean quit = false;

        //This sets the User ID and checks it against the customer table before moving to the next step.
            int userId=0;
            try {
                userId = userIsCustomer(connect, username);
            }
            catch(Exception e){
                //If the user ID is malformed, they are logged in but all entries return empty set. This is a feature.
                userId=1;
            }
        if ( userId != 0 ) {
            System.out.println("Welcome, customer");
            while ( !quit ) {
                quit = replCustomer(sc, myLS, connect, userId);
            }
        } else {
            while ( !quit ) {
                quit = repl(sc, myLS, terminator, change, connect);
            }
        }

        System.out.println("Bye!");

        sc.close();

        } catch (Exception e) {
            System.out.println("Program go bye bye");
            e.printStackTrace();
        }
    }

    private static int userIsCustomer ( Connection conn, String username ) throws Exception {
        Statement stmt = conn.createStatement();
        ResultSet rs = stmt.executeQuery("SELECT * FROM leatherstore.kostomers WHERE Email like" +
                "\"" + username + "@%\"");
        if ( rs.next() ) {
            return rs.getInt(1);
        } else {
            return 0;
        }
    }

    static boolean repl ( Scanner sc, LeatherSelect anLS, LeatherDelete anLD, LeatherUpdate anLU, Connection c ) throws Exception{
        System.out.print(menu);
        System.out.print(replPrompt);
        String cmd = sc.nextLine();
        return eval(cmd,sc, anLS, anLD, anLU, c);
    }

    private static boolean replCustomer ( Scanner sc, LeatherSelect anLS, Connection c, int userId ) throws Exception {
        System.out.print(menuCustomer);
        System.out.print(replPrompt);
        String cmd = sc.nextLine();
        return evalCustomer(cmd, sc, anLS, c, userId);
    }

    static boolean eval ( String cmd, Scanner sc, LeatherSelect myLS, LeatherDelete myTerminator, LeatherUpdate change,Connection c) throws Exception{
        if ( cmd.equals("1") || cmd.toLowerCase().equals("add") ) {
            add(sc, c);

        } else if ( cmd.equals("2") || cmd.toLowerCase().equals("modify") ) {
            modify(sc, c, change);
        } else if ( cmd.equals("3") || cmd.toLowerCase().equals("delete") ) {
            delete(sc, c);

        } else if ( cmd.equals("4") || cmd.toLowerCase().equals("view") ) {
           view(sc, c, myLS);
        }
         else if ( cmd.toLowerCase().equals("q") || cmd.toLowerCase().equals("quit") ) {
            return true;
        }
        else if ( cmd.equals("There is no spoon")) {
            ParserAssist.printLine("Welcome Deus Ex Machina\n what is reality?");Geck.groundHogsDay(c);return false;
        }
        return false;
    }

    private static boolean evalCustomer ( String cmd, Scanner sc, LeatherSelect myLS, Connection c, int userId ) throws Exception {
        if ( cmd.equals("1") || cmd.toLowerCase().equals("view") ) {
            viewCustomer(sc, c, myLS, userId);
        } else if ( cmd.toLowerCase().equals("q") || cmd.toLowerCase().equals("quit") ) {
            return true;
        }
        return false;
    }

    static void add ( Scanner sc, Connection conn ) {
        System.out.println("You asked me to add");
        try {
            LeatherInsert.processInsertRequest(sc, conn);
        } catch ( Exception e ) {
            e.printStackTrace();
        }
    }

    static void modify ( Scanner sc, Connection conn, LeatherUpdate change ) {
        System.out.println("You asked me to modify");
        try{
            change.processUpdate(sc, conn);

        } catch ( Exception e){
            System.out.println("Error modifying, please try again");
        }
    }

    //calls the deleteContextualizer class which manages the iterface for deletion
    static void delete ( Scanner sc, Connection c){

            DeleteContextualizer deleteMenu= new DeleteContextualizer();
            ParserAssist.printLine(deleteMenu.inititialize(sc,c));//This outputs the result of the deletion
 }

    static void viewCustomer ( Scanner sc, Connection c, LeatherSelect myLS, int userId ) {
        boolean check = false;

        while ( check == false ) {
            try {
                System.out.println("Enter the number for what you'd like to see:\n" +
                        "1. my info\n" +
                        "2. my orders\n" +
                        "3. go back"
                );
                System.out.print(replPrompt);
                int entry = Integer.parseInt(sc.nextLine());
                switch ( entry ) {
                    case 1:
                        myLS.spitOutDataCustomer("kostomers", c, userId);
                        check = true;
                        break;
                    case 2:
                        myLS.spitOutDataCustomer("orders", c, userId);
                        check = true;
                        break;
                    case 3:
                        check = true;
                        break;
                    default:
                }

            }
            catch ( Exception e ) {
                System.out.println("Invalid entry");
                e.printStackTrace();
            }
        }
    }

    static void view ( Scanner sc, Connection c, LeatherSelect myLS ) { //Sets up the View input/output loop
        //Set a boolean to exit the View Loop
        boolean check = false;

        while (check == false) {    //This loop exits the View loop when a view command is complete, or when a user uses the Go Back command
            try {
                TablePrint printy=new TablePrint(); //Get TablePrint object to print tables

                //Print Table names, menu items and prompt.
                System.out.println("Enter the number for what you'd like to see:");
                printy.printTables(c);
                int i=printy.tables.size()+1;
                System.out.println(i+". Additional Options");
                System.out.println((i+1)+". Go Back");
                System.out.print(replPrompt);

                //Take in entry and check it against options. Exceptions from entry caught by try/catch
                int entry=Integer.parseInt(sc.nextLine());
                if(entry==i){
                    //Take user to additional options submenu
                    myLS.additionalMenu(sc, c);
                }
                else if (entry <= printy.tables.size() && entry > 0){
                    //Take user to main view menu for the table name they entered.
                    myLS.spitOutData(printy.getTableName(entry - 1), c, sc);
                    //Boolean switch to exit
                    check = true;

                }
                else if (entry==i+1){
                    //exit view if this is selected
                    check=true;
                }
            } catch (Exception e) {
                //catch inside loop means that malformed entries will give an error message and resume the loop.
                System.out.println("Invalid entry. Please Try Again");
            }
        }
    }
}
