package com.ls20.simple;

import java.sql.Connection;

/**
 * Created by Th3 D1v1n3 5h4d0w (Bill Molchan)on 5/1/2014.
 * Garden of Eden Creation Kit
 * create life where non existed before
 */
public class Geck {
    //ThisFunction destroys all tables and then rebuilds them with initial values
    public static void groundHogsDay(Connection c){
        String error = initGeck(c);
        adamEve(c);
        //ParserAssist.printLine(error); uncomment this to print the query results for debugging
    }

    //This method drops all the tables and then reinstantiates them
    public static String initGeck(Connection c) {
        nuclearWinter(c);
        return rebuildAll(c);
    }

    //This function deletes all the tables
    public static void nuclearWinter(Connection c) {
        LeatherDelete nuclearWar = new LeatherDelete();
        nuclearWar.setCon(c);
        nuclearWar.slayTable("OrderBreakDown");
        nuclearWar.slayTable("Orders");
        nuclearWar.slayTable("Kostomers");
        nuclearWar.slayTable("Product");
        nuclearWar.slayTable("Address");
        nuclearWar.slayTable("Manufacturer");
        nuclearWar.slayTable("Category");

    }

    //This method instantiates all the tables in an order consistant with foreign key constraints
    public static String rebuildAll(Connection c) {
        String result = "";
        result = result + rebuildCategory(c);
        result = result + rebuildManufacturer(c);
        result = result + rebuildAddress(c);
        result = result + rebuildProduct(c);
        result = result + rebuildKostomers(c);
        result = result + rebuildOrder(c);
        result = result + rebuildOrderBreakDown(c);
        return result;
    }

    //This method populates all the tables with their initial values
    //values defined in populate methods
    public static void adamEve(Connection c){
        populateCategory(c);
        populateManufacturer(c);
        populateAddress(c);
        populateProduct(c);
        populateKostomer(c);
        populateOrder(c);
        populateOrderBreakDown(c);
    }

    // this collection of methods each reconstruct a table in case of deletion
    public static String rebuildManufacturer(Connection c) {
        String tableDef = "CREATE TABLE Manufacturer (\n" +
                "Manufacturer_ID INT NOT NULL AUTO_INCREMENT PRIMARY KEY,\n" +
                "Name VARCHAR(50) NOT NULL UNIQUE,\n" +
                "Logo_Location TINYTEXT,\n" +
                "Description TEXT,\n" +
                "Title TINYTEXT\n" +
                ");";
        return ParserAssist.executeCommand(c, tableDef);
    }
    public static String rebuildProduct(Connection c) {
        String tableDef = "CREATE TABLE Product (\n" +
                "Product_ID INT NOT NULL AUTO_INCREMENT PRIMARY KEY,\n" +
                "Product_Name VARCHAR(50) NOT NULL,\n" +
                "Product_Description TEXT,\n" +
                "Price_US DECIMAL(10,2) NOT NULL,\n" +
                "Quantity INT NOT NULL,\n" +
                "Model TinyText,\n" +
                "Product_Title TINYTEXT,\n" +
                "Weight_in_lb DECIMAL(6,3),\n" +
                "Image_location TEXT,\n" +
                "Status TINYTEXT,\n" +
                "Colour VARCHAR(15),\n" +
                "Manufacturer_ID INT,\n" +
                "Category_ID INT,\n" +
                "FOREIGN KEY (Manufacturer_ID)      REFERENCES Manufacturer (Manufacturer_ID) ON DELETE CASCADE ,\n" +
                "FOREIGN KEY (Category_ID)      REFERENCES Category(Category_ID) ON DELETE CASCADE ON UPDATE CASCADE\n" +
                ");";
        return ParserAssist.executeCommand(c, tableDef);
    }
    public static String rebuildCategory(Connection c) {
        String tableDef = "CREATE TABLE Category(\n" +
                "Category_ID INT NOT NULL AUTO_INCREMENT PRIMARY KEY,\n" +
                "Category_Name VARCHAR(15) NOT NULL,\n" +
                "Image_location_Category TEXT,\n" +
                "Parent_ID INT\n" +
                ");";
        String addParent = "INSERT INTO Category VALUES (1,'parent', 'noimg.jpg',1);";
        String fkeyConstraint = "Alter Table Category add FOREIGN KEY (Parent_ID)      REFERENCES Category (Category_ID) ON UPDATE CASCADE ON DELETE CASCADE;";
        String result = ParserAssist.executeCommand(c, tableDef);
        result = result + ParserAssist.executeCommand(c, addParent);
        return result + ParserAssist.executeCommand(c, fkeyConstraint);
    }
    public static String rebuildAddress(Connection c) {
        String tableDef = "CREATE TABLE\tAddress(\n" +
                "Address_ID INT NOT NULL AUTO_INCREMENT PRIMARY KEY,\n" +
                "House_Number INT NOT NULL,\n" +
                "Street_Name TINYTEXT NOT NULL,\n" +
                "City VARCHAR(15) NOT NULL,\n" +
                "State VARCHAR(15) NOT NULL,\n" +
                "ZipCode Int\n" +
                ");";
        return ParserAssist.executeCommand(c, tableDef);
    }
    public static String rebuildOrder(Connection c) {
        String tableDef = "CREATE TABLE\tOrders(\n" +
                "Order_ID INT NOT NULL AUTO_INCREMENT PRIMARY KEY,\n" +
                "Order_Date DATE NOT NULL,\n" +
                "Total_Price DECIMAL(10,2),\n" +
                "Order_Status TINYTEXT\t,\n" +
                "Shipping_Address INT,\n" +
                "Billing_Address INT,\n" +
                "User_ID INT,\n" +
                "FOREIGN KEY (Shipping_Address)      REFERENCES Address (Address_ID) ,\n" +
                "FOREIGN KEY (Billing_Address)      REFERENCES Address (Address_ID),\n" +
                "FOREIGN KEY (User_ID)      REFERENCES Kostomers (User_ID)\n" +
                ");";
        return ParserAssist.executeCommand(c, tableDef);
    }
    public static String rebuildKostomers(Connection c) {
        String tableDef = "CREATE TABLE\tKostomers(\n" +
                "User_ID INT NOT NULL AUTO_INCREMENT PRIMARY KEY,\n" +
                "Email TINYTEXT,\n" +
                "Phone_number TINYTEXT NOT NULL,\n" +
                "First_name VARCHAR(15) NOT NULL,\n" +
                "Last_name VARCHAR(15) NOT NULL,\n" +
                "Primary_Shipping_Address INT,\n" +
                "Primary_Billing_Address INT,\n" +
                "FOREIGN KEY (Primary_Shipping_Address)      REFERENCES Address (Address_ID) ON DELETE CASCADE,\n" +
                "FOREIGN KEY (Primary_Billing_Address)      REFERENCES Address (Address_ID) ON DELETE CASCADE\n" +
                ");";
        return ParserAssist.executeCommand(c, tableDef);
    }
    public static String rebuildOrderBreakDown(Connection c) {
        String tableDef = "CREATE TABLE\tOrderBreakDown(\n" +
                "Order_ID INT,\n" +
                "Product_ID INT,\n" +
                "Quantity INT,\n" +
                "PurchasePrice DECIMAL(10,2),\n" +
                "FOREIGN KEY (Order_ID)      REFERENCES Orders (Order_ID) ON DELETE CASCADE,\n" +
                "FOREIGN KEY (Product_ID)      REFERENCES Product (Product_ID) ON DELETE CASCADE\n" +
                ");";
        return ParserAssist.executeCommand(c, tableDef);
    }


    //This method processes an input request
    private static void processInsert(Connection c, String table,String values){
        ParserAssist.executeCommand(c, "INSERT INTO "+ table+ " VALUES (" + values + ");");
    }
    //these methods format data for the processInsert command
    private static void categoryInsert(Connection c,String values){processInsert(c,"Category",values);}
    private static void manufacturerInsert(Connection c,String values){processInsert(c,"Manufacturer",values);}
    private static void addressInsert(Connection c,String values){processInsert(c,"Address",values);}
    private static void productInsert(Connection c,String values){processInsert(c,"Product",values);}
    private static void kostomersInsert(Connection c,String values){processInsert(c,"Kostomers",values);}
    private static void orderInsert(Connection c,String values){processInsert(c,"Orders",values);}
    private static void orderBreakDownInsert(Connection c,String values){processInsert(c,"OrderBreakDown",values);}

    //These methods use the formulaters to add initial values to the tables
    public static void populateCategory(Connection c) {

        categoryInsert(c,"2,'Mens', 'men.jpg',1");
        categoryInsert(c,"3,'Ladies', 'women.jpg',1");
        categoryInsert(c,"4,'Briefcases', 'Brieficases.jpg',2");
        categoryInsert(c,"5,'Purses', 'purse.jpg',3");
        categoryInsert(c,"6,'Laptop Slot', 'Laptop.jpg',4");
        categoryInsert(c,"7,'Briefcases', 'Brieficases.jpg',3");
        categoryInsert(c,"8,'Laptop Slot', 'Laptop.jpg',7");

    }
    public static void populateManufacturer(Connection c) {

        manufacturerInsert(c,"1, 'Matrix Enterprises','/dont open/neo.jpg', 'A company dedicated to fabricating products so fantastic you will doubt their real', 'The manufacturer of Dreams'");
        manufacturerInsert(c,"2, 'Leather Lords','/dont open/deus.jpg', 'leather company', 'leather fit for a proper lady'");
        manufacturerInsert(c,"3, 'Crikey','/dont open/yum.jpg', 'company that produces pseudoanimal skins', 'Show your inner crocodile'");



    }
    public static void populateAddress(Connection c) {

        addressInsert(c,"1,271828, 'e lane', 'mathematica','Illionis', 60616");
        addressInsert(c,"2,630, 'center of road', 'hitme','Ohio', 44233");
        addressInsert(c,"3,666, 'banjo time', 'Fiddle', 'Georgia', 30002");
    }
    public static void populateProduct(Connection c) {

        productInsert(c,"1,'Amazo Bag', 'an ordinary bag, for men', 100.20, 100, 'amazing model', 'The bag of Awesome', 1.2, 'amazo.jpg', 'In Stock', 'Blue', 1, 1");
        productInsert(c,"2,'lame Bag', 'an lame bag, for women', 200, 10, 'lame model', 'The bag of  Lame', 25, 'amazo2.jpg', 'In Stock', 'Red', 1, 2");
        productInsert(c,"3,'Brief Brief, 'A Briefcase that looks like a pair of Briefs', 5, 15, 'only model', 'The Brief Brief',0.25, 'brief.jpg', 'Backorder', 'Maroon', 1, 3");
        productInsert(c,"4,'Biting purse', 'A Purse with a Snap', 100, 5, 'omonommmm', 'Consumer of your monies', 25, 'croc.jpg', 'In Stock', 'Red', 3, 4)");
        productInsert(c,"5,'Leather Lap', 'A laptop bag made of LEATHER ', 100, 5, 'SOFT', 'so soft', 25, 'leatherlaptop.jpg', 'In Stock', 'black',2, 7");



    }
    public static void populateKostomer(Connection c) {

        kostomersInsert(c,"1, 'ME@email.com', '3303412312', 'Bill', 'M', 1 ,1");
        kostomersInsert(c,"2, 'Chandler@cat.com', '1234567890', 'Cat', 'Meow', 1 ,2");
        kostomersInsert(c,"3, 'Fiddler@banjo.com', '6666666666', 'Lucifer', 'Gold', 3 ,3");
        kostomersInsert(c,"4, 'mrsmith@gmailcom', '1235556309', 'Mister', 'Smith', 2, 2");

    }
    public static void populateOrder(Connection c) {

        orderInsert(c,"1, '2012-11-11',1000.25,'backordered',1,1,1");
        orderInsert(c,"2, '2013-11-11',200.23,'Delivered',1,2,2");
        orderInsert(c,"3, '2014-01-01',5,'Delivered',2,1,1");
        orderInsert(c,"4, '2006-6-6',666.66,'Paid in gold Fiddle',3,3,3");


    }
    public static void populateOrderBreakDown(Connection c) {

        orderBreakDownInsert(c,"1,2,4,200");
        orderBreakDownInsert(c,"1,4,2,100");
        orderBreakDownInsert(c,"2,1,1,200.23");
        orderBreakDownInsert(c,"3,3,1,5");


    }



}