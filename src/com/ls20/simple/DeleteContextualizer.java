package com.ls20.simple;

import com.sun.corba.se.impl.orbutil.closure.Constant;

import javax.xml.transform.Result;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.Scanner;

/**
 * Created by Th3 D1v1n3 5h4d0w (Bill Molchan)on 4/30/2014.
 * The return type of the String classes is a message stating whether the operation was succesful and
 * what the operation was.
 *
 * To use this class, call it with the initialize function, it will return a string upon completion
 *
 * Static methods are called from the instantiation because it's easier to type and read output than ParserAssist
 */
public class DeleteContextualizer {
    //class varibles, these are initialized in the initialize function
    Scanner myEyes;
    Connection theConnection;
    ParserAssist output = new ParserAssist();
    LeatherDelete hercules;


    //These constants are universal across the entire program, they are recreated here to give the compiler faith they are constants
    //In particular these serwe to pass error messages back and forth from the parser to the calling function
    static final int parsefailFlag = ParserAssist.parsefailFlag;
    static final int cancelFlag = ParserAssist.cancelFlag;
    static final int notFound = ParserAssist.notFound;
    static final String stringFailFlag = ParserAssist.stringFailFlag;


    //This function initializes the class variables and then calls the main delete menu (menuStart)
    public String inititialize(Scanner c, Connection cs) {
        myEyes = c;
        theConnection = cs;
        output.printLine("You asked me to delete");
        hercules = new LeatherDelete();
        hercules.setCon(theConnection);
        return menuStart();
    }

    //This is the main delete menu, it prompts the user;
    //Then, after the input is validated, it is sent to processChoice to be acted on.
    private String menuStart() {
    output.printLine("\nWhat would you like to do?");
    output.printLine
            (
            "1. Delete Orders by date"+
            "\n2. Discontinue Empty Stock"+
            "\n3. Delete customer account"+
            "\n4. Cancel an order"+
            "\n5. Delete Tables or Entries" +
            "\n6. Cancel ");
    output.prompt();
    int choice = intVerify();//checks that the input is valid, returns a flag if something is wrong
    return processChoice(choice);//processes the selection
    }

    //This method processes the decision from the main menu and calls the appropriate function
    private String processChoice(int i)    {
    switch (i) {
        case 1: return deleteDate();//Delete orders by date
        case 2: return hercules.caurterize();//delete products out of stock (burn the severed hydra heads so they don't regrow)
        case 3: return removeCustomer();//gathers more input to delete a customer
        case 4: return revoke();//Cancels an order, requires additional information
        case 5: return precision();//launches series of selection methods
        case 6: return "Deletion Canceled";//cancel option, this message is returned all the
        case parsefailFlag: output.printLine("Your input could not be understood");//this flag means a non-number was entered
        default: output.printLine("Please Input a number from the menu");//If a number other than one in the list was selected the user is reprompted
            output.prompt();//the arrow indicating the user should respond
           return processChoice(intVerify());//This asks for a new number and then reprocesses it
     }
    }


    //Cancel an order
    private String revoke() {
    output.printLine("These are the current Orders");
    ParserAssist.showTable("Order",theConnection);//static method that lists all the tables
    output.printLine("Enter the ID of the order to Cancel:");
    output.prompt();//symbol to let user know to input information
        int i = intVerify();//validates input, returns flag if there is an error
        if (i==parsefailFlag) {output.badchoice();return revoke();}//if a non-integer is inputed, the user is asked to use numbers only and the instructions repeat
        String temp=hercules.revokeOrder(i);//attempts to cancel an order
        if (temp!=hercules.ns) {return temp;}//ns is key for failed, if the operation suceeded, the message is piped up
        output.orderNotFound();//if the order was not in the table, the user is informed
        output.canceled();// then they are sent to the previus menu with some guiding text
        return menuStart();//returns to main delete menu
    }


//the remove customer methods
    private String removeCustomer() {//remaves a customer
        output.warningOrder();
        output.promptCustomer();
        output.prompt();
        String input =myEyes.nextLine().trim();
        if(input.equals("0")){return "delete canceled";}
        String response=hercules.casualty(input);//attempts to delete a customer, throws a flag upon failure
        if (response ==stringFailFlag){output.customerNotFound();output.canceled();return menuStart();}//if there is a failure flag, informs the user
        return response;// pipes a message Telling the user they were succesful
    }



    //the delete date methods delete old orders based on age
    private String deleteDate() {
        output.printLine("\nDelete orders older than how many days?");
        output.printLine(
                          "1. 30 days"+
                        "\n2. 60 days"+
                        "\n3. 90 days"+
                        "\n4. Custom date"+
                        "\n5. Cancel ");
        output.prompt();//tells user to input response
        int choice = intVerify();//validates entry
        int expireLength = parseDate(choice);//turns selection into corresponding integer
        return deleteExpiredParse(expireLength);//pipes a message depicting the user's success from the method that calls the deletion
    }

    //takes the input from deletDate and turns it into a number of days, or calls a function to parse the users custom timeframe
    private int parseDate(int time) {
        switch (time) {
            case 1: return 30;
            case 2: return 60;
            case 3: return 90;
            case 4: return customDateParse(time);
            case 5: return cancelFlag;
            case parsefailFlag: return  parsefailFlag;
            default: return notFound;
        }
    }

    //if custom date is selected in deleteDate, parseDate will call this to prompt the user and validate their response
    private int customDateParse(int time)    {
        output.promptExpireAge();
        int age =intVerify();//validates response
        if(age==parsefailFlag){output.badchoice();return parseDate(time);}//if a non-int is selected the user is reprompted at the previus level
        if (age<0) {return notFound;}
        return age;//return the parsed result
    }

    //This interprets the date from the parsedate and then calls a function to manage the deletion
    private String deleteExpiredParse(int time) {
        switch (time) {
            case parsefailFlag://a non-integer was inputed
                output.badchoice();
            case notFound://the entry was not found, or a negative number was inputed
                output.menuInstructions();
                return deleteDate();
                case cancelFlag:output.canceled(); return menuStart();
                default: return deleteExpired(time);//if everything was done correctly, attempt the deletion
        }
    }

    //deletes orders older than selected time
    private String deleteExpired(int time){//calls the deletion funciton

        return hercules.sanitize(time);
       }

    //Critically important function:IntVerify
    //If the user inputs a value corresponding to a flag, their input is incorrect
    //by convention, 0 is never an option, therefore the user will newer select something incorrect
    //therefore if a flag is parsed, it acts as if the input was zero, if a non-int is entered, it throws a flag
    int intVerify () {
        try {int i = Integer.parseInt(myEyes.nextLine().trim());
            switch (i) {
            case parsefailFlag:
            case cancelFlag:
                case notFound:
                    return 0;//ensuring that the user won't inadvertatly send a flag signal
                default:return i;//if everthing works, return the number

            }
        }
    catch (Exception e) {
        return parsefailFlag;//if a non-int is selected send a flag
    }
    }

    //This function narrows down where the user would like to delete from, it will loop upon invalid entry (with a message)
    //Sends table to deleteMaster to find the rows targeted for deletion
    private String precision() {
        output.tableListIntro();
        output.listTables(theConnection);
        output.promptTable();
        String selectedTable = sanitizeTable(myEyes.nextLine().trim().toLowerCase());//ensures that the table is in the database

        if (selectedTable == stringFailFlag) {//if the table was not found, this flag will be sent
            output.tableNotFound();
            output.prompt();
            myEyes.nextLine();
            return precision();//loop upon failure (after printing explanation)
        }
        return deleteMaster(selectedTable);
    }

    //This ensures a table is in the database, it is called by precision
    private String sanitizeTable(String tableName){
        try {
            ResultSet tables = ParserAssist.getTables(theConnection);//gets a list of all the tables
            while (tables.next()) {//while tables have yet to be checked
                if (tables.getString("Table_name").trim().toLowerCase().equals(tableName.trim().toLowerCase())) {
                    return tableName;
                }

            }
            return stringFailFlag;//sends flag on failure
        }
        catch(Exception e){return stringFailFlag+e.getMessage();}//sends flag with description, this should never have to happen. It would imply a database error

    }

    //called by precision, specifies what part of table the user wants deleted and calls corresponding function
    private String deleteMaster(String victim) {
        output.printLine("\nWhich do you want to Delete?");
        output.printLine
                (
                        "1. Specific entries in a table" +
                                "\n2. All entries in a table " +
                                "\n3. The entire table" +
                                "\n4. Cancel"
                );
        int choice = intVerify();
        switch (choice) {
            case 1:
                return continueSelection(victim);
            case 2:
                return hercules.disembowelTable(victim);
            case 3:
                return hercules.slayTable(victim);
            case 4:
                output.canceled();
                return menuStart();
            case parsefailFlag:
                output.badchoice();
            default:
                output.menuInstructions();
                return deleteMaster(victim);
        }


    }

    //called by deletMaster, this function prompts the user for the primary key of the entry to deleted
    private String continueSelection(String victim){
        output.showTable(victim,theConnection);//shows contents of table
        String primarykey= ParserAssist.primaryKeyFinder(victim,theConnection);//finds primary key
        output.printLine("Please enter the "+ primarykey + "of the entry you would like to delete");
        output.prompt();
        String userRequest = myEyes.nextLine().trim();
        String result =hercules.arrow(userRequest,victim,primarykey);
        //if the result is found and deleted, return a description of the delete
        if (result.equals(hercules.sd)){return result;}

        //result was not found
        if (result.equals(hercules.ns)){output.printLine("The "+ primarykey + "you selected was not found");}
        else {
            String[] fkerror = result.split("`");
            String tableName = fkerror[3];
            //String fkField = fkerror[7]; not neccassary anymore
            String alias = fkerror[11];
            String response = "The entry you tried to delete contains necessary information for the " + tableName +
                    " table" +
                    "\n You will need to delete all instances of '" + userRequest + "' in the '"+ alias+"' column of '"+ tableName+ "' before attempting this delete";
            return response;
        }

        output.canceled();
        return deleteMaster(victim);//upon failure go up a level




    }












}
