package com.ls20.simple;

import java.sql.*;

/**
 * Created by Th3 D1v1n3 5h4d0w (Bill Molchan))on 4/16/2014.
 * setCon MUST be the first function used. It probably could get made into a constructer even
 * ns represents failure and is used as a flag is some other classes
 */
public class LeatherDelete {
    //class variables, only connection needs instantiation
    private Connection connect = null;
    private Statement statement = null;
    private PreparedStatement preparedStatement = null;
    private ResultSet resultSet = null;
    public final String sd = " Successfully Deleted";
    public final String ns = " Delete was not Successful";
    private Connection conn;


    public void setCon(Connection c){conn = c;}//sets the connection to the database

    //this function drops a table
    public String slayTable(String tableName) {
        try {
            statement=conn.createStatement();
            tableName=tableName.trim();
            statement.executeUpdate("DROP TABLE leatherstore."+tableName);
            return "Table"+ sd;
        }
        catch(Exception E){
            E.printStackTrace();
            return ns;
        }

    }

    //Deletes all elements in a table
    public String disembowelTable(String tableName)  {
        try {
            statement=conn.createStatement();
            tableName=tableName.trim();
            statement.executeUpdate("DELETE FROM leatherstore."+tableName);
            return "Contents of Table" +sd;
        }
        catch(Exception E){
            E.printStackTrace();
            return ns;
        }

    }

    //deletes all orders older than the input time in days
    public String sanitize(int time)  {
        try {
            statement=conn.createStatement();
            statement.executeUpdate("DELETE FROM leatherstore.Order WHERE Order_Date < (Now() - Interval "+time + " Day)");

            return "Orders older than "+time+ " days" +sd;
        }
        catch(Exception E){
            E.printStackTrace();
            return ns;
        }

    }

    //revokes an order with id given as argument
    public String revokeOrder(int orderID)  {
        try {
            statement=conn.createStatement();
            statement.executeUpdate("DELETE FROM leatherstore.Order WHERE Order_ID ="+orderID);
            return "Order "+ orderID+sd;
        }
        catch(Exception E){
            E.printStackTrace();
            return ns;
        }

    }

    //deletes all products that are out of stock
    public String caurterize(){
        try {
            statement=conn.createStatement();
            statement.executeUpdate("DELETE FROM leatherstore.Product WHERE Quantity = 0");
            return "Empty stock successfully deleted";
        } catch (Exception e){return "Query Failed"+e;}
    }

    //Deletes a customer
    public String casualty(String victim) {
        try {
            statement=conn.createStatement();
            ParserAssist.executeCommand(conn,"DELETE FROM leatherstore.Orders WHERE USER_ID ="+victim);
            statement.executeUpdate("DELETE FROM leatherstore.Kostomers WHERE User_ID ="+victim);
            return "Kostomer"+sd;
        } catch (Exception e){return "Query Failed"+e;}

    }

    //Deletes an arbitrary entry (target) from the table (tableName) using the primary key (pk).
    //target is the value of the primary key on the row marked for deletion, pk is the attribute name
    //there is a function is parseassist for finding primarykey names
      public String arrow(String target,String tableName,String pk){
        try{
            statement=conn.createStatement();
            statement.executeUpdate("DELETE FROM leatherstore."+tableName+" WHERE "+pk+" = "+ target);
            return sd;
        }
        catch (SQLIntegrityConstraintViolationException e){return e.getMessage();}
        catch (Exception e ){return ns;}


    }



}
