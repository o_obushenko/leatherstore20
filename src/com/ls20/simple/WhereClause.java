package com.ls20.simple;

import java.sql.*;
import java.util.Scanner;

/**
 * Created by clem on 4/30/14.
 */

/**
 * The only public method in this class is
 * makeWhereClause(Connection,Scanner,ResultSetMetaData).
 * When called, it will prompt the user using menus
 * to create a WHERE clause for SELECT, DELETE, etc.
 * Pass in the Connection and Scanner being used, as
 * well as ResultSetMetaData for the table being
 * queried. It will either return a WHERE clause,
 * or an empty string, if the user changes his mind
 * about making a WHERE clause.
 *  -clem
 */


public class WhereClause {

    static String prompt = "> ";

    public static String makeWhereClause ( Connection conn, Scanner sc, ResultSetMetaData rsmd ) throws Exception {
        String whereClause = "where ";
        System.out.println("Which column do you want a constraint for?");
        printColumns(rsmd);
        System.out.print(prompt);

        // asks user which column he wants to add a constraint for

        int choice = Integer.parseInt(sc.nextLine());
        int type = rsmd.getColumnType(choice);
        String name = rsmd.getColumnName(choice);
        if ( type == Types.LONGNVARCHAR || type == Types.LONGVARCHAR ||
                type == Types.NVARCHAR || type == Types.VARCHAR ) {

            // there are different possible constraints depending on the type of the column

            System.out.println(name + " is a string");
            whereClause += name + " ";
            String constraint = stringConstraint(sc);

            // the method called will return an empty string if there is no where clause

            if ( !constraint.equals("") ) {
                return whereClause + constraint;
            }
            else {
                return "";
            }
        } else if ( type == Types.INTEGER ) {
            System.out.println(name + " is a number");
            whereClause += name + " ";
            String constraint = numberConstraint(sc);
            if ( !constraint.equals("") ) {
                return whereClause + constraint;
            }
        }
        return "";
    }

    private static String stringConstraint ( Scanner sc ) {
        String constraint = "";
        System.out.println(
                "1. matches exactly\n" +
                "2. does not match exactly\n" +
                "3. contains\n" +
                "4. does not contain\n" +
                "5. never mind"
        );
        System.out.print(prompt);
        String choice = sc.nextLine();

        // prompts user for the kind of constraint he wants, and puts
        // the correct keyword(s) in the query

        char c = choice.charAt(0);
        boolean contains = false;
        switch ( c ) {
            case '1':
                constraint += "LIKE ";
                break;
            case '2':
                constraint += "NOT LIKE ";
                break;
            case '3':
                constraint += "LIKE ";
                contains = true;
                break;
            case '4':
                constraint += "NOT LIKE ";
                contains = true;
                break;
            case '5':
                return "";
            default:
                System.out.println("Invalid input, try again");
                return stringConstraint(sc);
        }
        if ( contains ) {
            return constraint + "\"%" + constraintInput(sc) + "%\"";
        }
        else {
            return constraint + "\"" + constraintInput(sc) + "\"";
        }
    }

    private static String numberConstraint ( Scanner sc ) {
        String constraint = "";
        System.out.println(
                        "1. greater than\n" +
                        "2. less than\n" +
                        "3. equal to\n" +
                        "4. not equal to\n" +
                        "5. never mind"
        );
        System.out.print(prompt);

        // prompts user for the kind of constraint he wants
        // and puts the correct keyword(s) in the query

        String choice = sc.nextLine();
        char c = choice.charAt(0);
        switch ( c ) {
            case '1':
                constraint += ">";
                break;
            case '2':
                constraint += "<";
                break;
            case '3':
                constraint += "=";
                break;
            case '4':
                constraint += "<>";
                break;
            case '5':
                return "";
            default:
                System.out.println("Invalid input, try again");
                return numberConstraint(sc);
        }
        return constraint + constraintInput(sc);
    }

    private static String constraintInput ( Scanner sc ) {
        System.out.println("What do you want to constrain to?");
        System.out.print(prompt);
        // returns whatever the user typed
        return sc.nextLine();
    }

    private static void printColumns ( ResultSetMetaData rsmd ) throws Exception {
        int i = 0;
        for ( i = 1; i <= rsmd.getColumnCount(); i++ ) {
            // iterates through the columns and prints them
            System.out.println(i + ". " + rsmd.getColumnName(i));
        }
    }
}
