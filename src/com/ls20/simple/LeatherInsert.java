package com.ls20.simple;

/**
 * Created by clem on 4/19/14.
 */

import java.sql.*;
import java.util.Scanner;

/*
 * This class will construct and call a query to
 * insert into the database.
 */

public class LeatherInsert {

    public static void processInsertRequest ( Scanner sc, Connection conn ) throws Exception {
        try {
            TablePrint tp = new TablePrint();
            tp.printTables(conn);

            System.out.println("Which table would you like to add to?");
            System.out.print("> ");

            int selection = sc.nextInt();
            sc.nextLine();

            // figures out which table the user wants to insert into

            String selectedTable = tp.getTableName(selection - 1);
            Statement stmt = conn.createStatement();
            ResultSet resultSet = stmt.executeQuery("select * from LEATHERSTORE." + selectedTable);
            // get meta data to pass to the next method
            ResultSetMetaData resultSetMetaData = resultSet.getMetaData();

            insertToTable(sc, conn, resultSetMetaData);
        }
            catch(Exception e){
                System.out.println("Invalid Entry");
                sc.nextLine();
            }
    }

    private static void insertToTable ( Scanner sc, Connection conn, ResultSetMetaData resultSetMetaData ) throws Exception{

            String tableName = resultSetMetaData.getTableName(1);
            String query = "insert into " + "leatherstore." + tableName + " values (";
            System.out.println("Adding to " + tableName);
            for (int i = 1; i <= resultSetMetaData.getColumnCount(); i++) {

                // iterate through columns of results and prompts user for
                // data to input

                System.out.print(resultSetMetaData.getColumnName(i));
                switch ( resultSetMetaData.getColumnType(i) ) {
                    case Types.LONGNVARCHAR:
                    case Types.LONGVARCHAR:
                    case Types.VARCHAR:
                    case Types.NVARCHAR:
                        System.out.print(" (string) ");
                        break;
                    case Types.INTEGER:
                        System.out.print(" (integer) ");
                        break;
                    default:
                        break;
                }
                System.out.print(": ");
                String val = sc.nextLine().trim();
                int columnType = resultSetMetaData.getColumnType(i);
                if (columnType == Types.LONGNVARCHAR || columnType == Types.LONGVARCHAR ||
                        columnType == Types.VARCHAR || columnType == Types.NVARCHAR || columnType == Types.DATE) {

                    // prepends and appends datum with quotes if it's a string

                    query += "\"";
                    query += val;
                    query += "\"";
                } else {
                    query += val;
                }
                if (i < resultSetMetaData.getColumnCount()) {
                    query += ",";
                }
            }
            query += ");";
            Statement stmt = conn.createStatement();
            try {
                stmt.executeUpdate(query);
            } catch (Exception e) {
                System.out.println("problem inserting");
            }

            System.out.println("Would you like to :\n" +
                    "1. Insert another row" +
                    "\t2. Go back to the menu");
            System.out.print("> ");
            if (sc.nextLine().equals("1")) {
                insertToTable(sc, conn, resultSetMetaData);
            }
        }



    public static void insertIntoTable ( String tableName, Connection conn ) throws Exception {

    }
}
