package com.ls20.simple;

import java.sql.*;
import java.util.ArrayList;

/**
 * Created by Oless on 4/22/2014 to save your ass.
 */
public class TablePrint {

    private static Statement statement = null;
    private static ResultSet results = null;
    static ArrayList<String> columns=new ArrayList<String>();
    static ArrayList<String> tables=new ArrayList<String>();

        //Prints table names
    static void printTables(Connection conn) throws Exception {
        try {
            //make a statement, and send query to get table names to DB.
            statement = conn.createStatement();
            results = statement.executeQuery("SELECT TABLE_NAME\n" +
                    "FROM information_schema.TABLES \n" +
                    "WHERE TABLE_SCHEMA = 'leatherstore'");
            //iterator. 1 is the first in every resultSet
            int i=1;
            //If there are tables, iterate through the result set and print them out. Save them to the String arraylist for easy access later, too.
            if(tables.isEmpty()) {
                while (results.next()) {
                    tables.add(results.getString("Table_Name"));
                    System.out.println(i + ". " + results.getString("Table_Name"));
                    i++;
                }
            }
            //Otherwise, try to do it anyway.
            else {
                while (results.next()) {
                    System.out.println(i + ". " + results.getString("Table_Name"));
                    i++;
                }
            }
        }
        //Helpful Error Message
        catch(Exception E){
            System.out.println("Error Printing.");
        }
    }

    //Print column names for a table
    static void printColumns(Connection conn) throws Exception {
        try {
            //Create a Statement and send it
            statement = conn.createStatement();
            results = statement.executeQuery("SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = 'leatherstore' AND TABLE_NAME = 'order';");

            //Iterator=1 for the same reason as the previous method
            int i=1;
            //Get the results and save/print them exactly as in previous method
            while (results.next()) {
                columns.add(results.getString("COLUMN_NAME"));
                System.out.println(i+". "+results.getString("COLUMN_NAME"));
                i++;
            }
        }

        //necessary helpful error message
        catch(Exception E){
            System.out.println("Error Printing.");
        }
    }

    //Using an index, retrieve column name. Great for menus. Hint Hint.
    static String getColumnName(int n){
        return columns.get(n);
    }


    //Using an index, retrieve table name. Great for menus. Hint Hint.
    static String getTableName(int n){
        return tables.get(n);
    }


    //Universally useful results-printing method. Nice and short, too. Throw it a result set and it will print a list of results.
    static void printResults(ResultSet results) throws Exception{
        //get the result set's metadata
        ResultSetMetaData rsmd=results.getMetaData();
        //Iterate through to the end.
        while(results.next()){
            //New line between input/output/entries
            System.out.print("\n");
            //Print all entries in a row with their column names.
            for(int i=1;i<=rsmd.getColumnCount();i++){
                //getColumnName gets column name from metadata. Using that, you can also get and print any kind of object as a string given column name. Except maybe pictures.
                System.out.println(rsmd.getColumnName(i)+": "+results.getString(rsmd.getColumnName(i)));
            }
            //newline spacing.
            System.out.print("\n");
        }

    }

}
